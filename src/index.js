export function Store (validate) {
  let listeners = []
  let value
  let prev

  const view = fn => fn(value)

  view.set = fn => {
    const next = fn(value)
    if (validate) validate(next)
    prev = value
    value = next
    listeners.forEach(x => {
      const a = x.viewFn(value)
      const b = x.viewFn(prev)
      if (a !== b) x.cb(a, b)
    })
  }

  view.unsubscribe = cb => {
    listeners = listeners.filter(x => x.cb !== cb)
  }

  view.subscribe = (viewFn, cb, opts = {}) => {
    view.unsubscribe(cb)
    listeners.push({ viewFn, cb })
    if (opts.immediate) cb(viewFn(value), viewFn(prev))
    return () => view.unsubscribe(cb)
  }

  return view
}
