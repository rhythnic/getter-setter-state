const { Store } = require('../dist/index.js')
const R = require('ramda')

describe('Store', () => {
	it('uses getters and setters to interact with state', () => {
		const store = Store()
		store.set(R.always({ a: 0 }))
		expect(store(R.prop('a'))).toBe(0)
	})
	it('can be called with a validation function', () => {
		const validate = () => { throw new Error('Invalid') }
		const store = Store(validate)
		expect(() => store.set(R.always({ a: 0 }))).toThrow()
	})
	it('can be subscribed to', () => {
		const store = Store()
		const lens = R.lensProp('a')
		let count = 0
		const unsubscibe = store.subscribe(R.view(lens), val => {
			expect(val).toBe(1)
			count += 1
			unsubscibe()
		})
		store.set(R.always({ a: 1 }))
		store.set(R.set(lens, 2))
		expect(count).toBe(1)
	})
})
